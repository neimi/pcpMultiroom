# pcpMultiroom

## Software-Installation

piCorePlayer-Image downloaden und auf SD-Karte schreiben
https://docs.picoreplayer.org/downloads/

Karte in den Pi und Strom drauf...

Browser öffnen und die IP oder wenn schon DNS eingerichtet ist den Namen in die Adresszeile eingeben.
Jetzt sollte die Weboberfläche des piCorePlayer erscheinen.

## erster Test

Für einen ersten Test im Webinterface auf den Reiter "Squeezelite Settings" wechseln und unter LMS IP euren Logitechmediaserver eintragen (z.B. 192.168.1.73 oder logitechmediaserver)


## Multiroom Konfiguration

via ssh am Raspberry Pi anmelden
    ssh tc@pcpmultiroom
    tc@pcpmultiroom's password: piCore



    sudo mv /etc/asound.conf ~/asound.conf.original
    sudo wget https://codeberg.org/neimi/pcpMultiroom/raw/branch/main/asound.conf -P /etc


    sudo mv /opt/bootlocal.sh /opt/bootlocal.sh.original
    sudo wget https://codeberg.org/neimi/pcpMultiroom/raw/branch/main/bootlocal.sh -P /opt
    sudo chmod 775 /opt/bootlocal.sh

    sudo wget https://codeberg.org/neimi/pcpMultiroom/raw/branch/main/muting_power.sh -P ~

Nun die Adresse/IP des Logitechmediaservers anpassen - natürlich muss hier die musik.domain.local mit der Adresse/IP des LMS ersetzt werden

    sed -i 's/logitechmediaserver/musik.domain.local/g' /opt/bootlocal.sh  /home/tc/muting_power.sh

Änderungen speichern mit

    sudo filetool.sh -b

Neustart

    sudo reboot

sehen ob/welche/wieviele Squeezelite-Instanzen laufen

    ps | grep squeeze

squeezelite-Instanzen beenden

    sudo kill 16376

# Standardlautstärke ändern:

Zuerst die ID eurer Soundkarte herausfinden

```
cat /proc/asound/cards
 0 [Headphones     ]: bcm2835_headphonbcm2835 Headphones - bcm2835 Headphones
                      bcm2835 Headphones
 1 [Device         ]: USB-Audio - USB Sound Device
                      USB Sound Device at usb-3f980000.usb-1.2, full speed
 2 [Device_1       ]: USB-Audio - USB Sound Device
                      USB Sound Device at usb-3f980000.usb-1.3, full speed
```
Alsamixer mit der ID starten
```
export TERM=xterm && alsamixer -D hw:1
```
Nun die Werte anpassen (habe bei mir alle auf 0 heruntergedreht)

anschliessend die gemachten Einstellungen speichern
```
sudo alsactl store
```
um die Einstellungen beim Booten zu laden folgende Zeile in der /opt/bootlocal.sh ergänzen(ist in der bootlocal.sh des Repos bereits drin)
```
sudo alsactl restore
```
Änderungen dann wieder speichern:
```
sudo filetool.sh -b
```
ein Boot tut gut...
```
sudo reboot
```