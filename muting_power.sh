#!/bin/sh -x
#/home/tc/www/cgi-bin/pcp-functions
#pcp_variables

##############################################
# Set the following according to your setup
##############################################
LMS_IP=musik                    # LMS IP address
INTERVAL=1                      # Set Poll interval
COMMAND="status 0 0"            # LMS player status command
DELAYOFF=3                      # Delay in no. of intervals

##############################################
# Card 1
##############################################

Card1_Channel1_MAC_ADDR=ab:cd:ef:12:34:60     # MAC address of player for Card 1 channel 1 (stereo) Essen U1
Card1_Channel2_MAC_ADDR=ab:cd:ef:12:34:62     # MAC address of player for Card 1 channel 2 (stereo) Kochen U2
Card1_Channel3_1_MAC_ADDR=ab:cd:ef:12:34:64   # MAC address of player for Card 1 channel 3.1 (mono) Gang EG U3 links
Card1_Channel3_2_MAC_ADDR=ab:cd:ef:12:34:65   # MAC address of player for Card 1 channel 3.2 (mono) WC U3 rechts
Card1_Channel4_1_MAC_ADDR=ab:cd:ef:12:34:66   # MAC address of player for Card 1 channel 4.1 (mono) Waschen U4 links
Card1_Channel4_2_MAC_ADDR=ab:cd:ef:12:34:67   # MAC address of player for Card 1 channel 4.2 (mono) Hobby U4 rechts

Card1_Channel1_GPIO=11         # GPIO for muting Card 1 channel 1
Card1_Channel2_GPIO=12         # GPIO for muting Card 1 channel 2
Card1_Channel3_GPIO=13         # GPIO for muting Card 1 channel 3
Card1_Channel4_GPIO=14         # GPIO for muting Card 1 channel 4

Card1_Channel1_COUNT=0                  # Delay counter for muting Card 1 channel 1
Card1_Channel2_COUNT=0                  # Delay counter for muting Card 1 channel 2
Card1_Channel3_COUNT=0                  # Delay counter for muting Card 1 channel 3
Card1_Channel4_COUNT=0                  # Delay counter for muting Card 1 channel 4

Card1_Channel1_TURNED_ON=0      # Status of player(s) from Card 1 channel 1
Card1_Channel2_TURNED_ON=0      # Status of player(s) from Card 1 channel 2
Card1_Channel3_TURNED_ON=0      # Status of player(s) from Card 1 channel 3
Card1_Channel4_TURNED_ON=0      # Status of player(s) from Card 1 channel 4

Card1_GPIO_PWR=10                               # GPIO for power on/off
Card1_DELAYOFFPWR=60            # Delay time in no. of intervals for power off
Card1_COUNTPWR=0                                # Delay counter for power off
Card1_TURNED_ONPWR=0                    # Status of power on/off

##############################################
# Card 2
##############################################


Card2_Channel1_1_MAC_ADDR=ab:cd:ef:12:34:70   # MAC address of player for Card 2 channel 1.1 (mono) Gang EG U3 links
Card2_Channel1_2_MAC_ADDR=ab:cd:ef:12:34:71   # MAC address of player for Card 2 channel 1.2 (mono) WC U3 rechts
Card2_Channel2_1_MAC_ADDR=ab:cd:ef:12:34:72   # MAC address of player for Card 2 channel 2.1 (mono) Gang EG U3 links
Card2_Channel2_2_MAC_ADDR=ab:cd:ef:12:34:73   # MAC address of player for Card 2 channel 2.2 (mono) WC U3 rechts
Card2_Channel3_1_MAC_ADDR=ab:cd:ef:12:34:74   # MAC address of player for Card 2 channel 3.1 (mono) Gang EG U3 links
Card2_Channel3_2_MAC_ADDR=ab:cd:ef:12:34:75   # MAC address of player for Card 2 channel 3.2 (mono) WC U3 rechts
Card2_Channel4_1_MAC_ADDR=ab:cd:ef:12:34:76   # MAC address of player for Card 2 channel 4.1 (mono) Waschen U4 links
Card2_Channel4_2_MAC_ADDR=ab:cd:ef:12:34:77   # MAC address of player for Card 2 channel 4.2 (mono) Hobby U4 rechts

Card2_Channel1_GPIO=21         # GPIO for muting Card 2 channel 1
Card2_Channel2_GPIO=22         # GPIO for muting Card 2 channel 2
Card2_Channel3_GPIO=23         # GPIO for muting Card 2 channel 3
Card2_Channel4_GPIO=24         # GPIO for muting Card 2 channel 4

Card2_Channel1_COUNT=0                  # Delay counter for muting Card 2 channel 1
Card2_Channel2_COUNT=0                  # Delay counter for muting Card 2 channel 2
Card2_Channel3_COUNT=0                  # Delay counter for muting Card 2 channel 3
Card2_Channel4_COUNT=0                  # Delay counter for muting Card 2 channel 4

Card2_Channel1_TURNED_ON=0      # Status of player(s) from Card 2 channel 1
Card2_Channel2_TURNED_ON=0      # Status of player(s) from Card 2 channel 2
Card2_Channel3_TURNED_ON=0      # Status of player(s) from Card 2 channel 3
Card2_Channel4_TURNED_ON=0      # Status of player(s) from Card 2 channel 4

Card2_GPIO_PWR=20                               # GPIO for power on/off
Card2_DELAYOFFPWR=60            # Delay time in no. of intervals for power off
Card2_COUNTPWR=0                                # Delay counter for power off
Card2_TURNED_ONPWR=0                    # Status of power on/off

##############################################



get_mode() {
  ##############################################
  # Card 1 Channel 1
  ##############################################
  RESULT=`echo "$Card1_Channel1_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
  echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1

  if [ $? = 0 ]; then
    echo "Playing $Card1_Channel1_MAC_ADDR. Unmute GPIO$Card1_Channel1_GPIO. Count: $Card1_Channel1_COUNT"
    Card1_Channel1_COUNT=0
    if [ $Card1_Channel1_TURNED_ON = 0 ]; then
        if [ $Card1_TURNED_ONPWR = 0 ]; then
          sudo echo "1" > /sys/class/gpio/gpio$Card1_GPIO_PWR/value
          Card1_TURNED_ONPWR=1
          echo "Power on!"
        fi
        sudo echo "1" > /sys/class/gpio/gpio$Card1_Channel1_GPIO/value
        Card1_Channel1_TURNED_ON=1
        echo "Turn on: $Card1_Channel1_GPIO"
    fi
  else
    if [ $Card1_Channel1_COUNT -ge $DELAYOFF ]; then
      if [ $Card1_Channel1_TURNED_ON = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel1_GPIO/value
        Card1_Channel1_TURNED_ON=0
        echo "Turn off: $Card1_Channel1_GPIO"
      fi
      Card1_Channel1_COUNT=0
    else
      Card1_Channel1_COUNT=$(($Card1_Channel1_COUNT + 1))
      echo "Stopped $Card1_Channel1_MAC_ADDR. Mute GPIO$Card1_Channel1_GPIO. . Count: $Card1_Channel1_COUNT"
    fi
  fi

  ##############################################
  # Card 1 Channel 2
  ##############################################
  RESULT=`echo "$Card1_Channel2_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
  echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
 
  if [ $? = 0 ]; then
    echo "Playing $Card1_Channel2_MAC_ADDR. Unmute GPIO$Card1_Channel2_GPIO. Count: $Card1_Channel2_COUNT"
    Card1_Channel2_COUNT=0
    if [ $Card1_Channel2_TURNED_ON = 0 ]; then
        if [ $Card1_TURNED_ONPWR = 0 ]; then
          sudo echo "1" > /sys/class/gpio/gpio$Card1_GPIO_PWR/value
          Card1_TURNED_ONPWR=1
          echo "Power on!"
        fi
        sudo echo "1" > /sys/class/gpio/gpio$Card1_Channel2_GPIO/value
        Card1_Channel2_TURNED_ON=1
        echo "Turn on: $Card1_Channel2_GPIO"
    fi
  else
    if [ $Card1_Channel2_COUNT -ge $DELAYOFF ]; then
      if [ $Card1_Channel2_TURNED_ON = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel2_GPIO/value
        Card1_Channel2_TURNED_ON=0
        echo "Turn off: $Card1_Channel2_GPIO"
      fi
      Card1_Channel2_COUNT=0
    else
      Card1_Channel2_COUNT=$(($Card1_Channel2_COUNT + 1))
      echo "Stopped $Card1_Channel2_MAC_ADDR. Mute GPIO$Card1_Channel2_GPIO. . Count: $Card1_Channel2_COUNT"
    fi
  fi

  ##############################################
  # Card 1 Channel 3
  ##############################################
  RESULT=`echo "$Card1_Channel3_1_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
  echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  # Wenn 1. Kanal nicht läuft, prüfen ob zweiter Kanal läuft
  if [ $? = 1 ]; then
        RESULT=`echo "$Card1_Channel3_2_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
        echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  fi

  if [ $? = 0 ]; then
    echo "Playing $Card1_Channel3_1_MAC_ADDR or $Card1_Channel3_2_MAC_ADDR. Unmute GPIO$Card1_Channel3_GPIO. Count: $Card1_Channel3_COUNT"
    Card1_Channel3_COUNT=0
    if [ $Card1_Channel3_TURNED_ON = 0 ]; then
        if [ $Card1_TURNED_ONPWR = 0 ]; then
          sudo echo "1" > /sys/class/gpio/gpio$Card1_GPIO_PWR/value
          Card1_TURNED_ONPWR=1
          echo "Power on!"
        fi
        sudo echo "1" > /sys/class/gpio/gpio$Card1_Channel3_GPIO/value
        Card1_Channel3_TURNED_ON=1
        echo "Turn on: $Card1_Channel3_GPIO"
    fi
  else
    if [ $Card1_Channel3_COUNT -ge $DELAYOFF ]; then
      if [ $Card1_Channel3_TURNED_ON = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel3_GPIO/value
        Card1_Channel3_TURNED_ON=0
        echo "Turn off: $Card1_Channel3_GPIO"
      fi
      Card1_Channel3_COUNT=0
    else
      Card1_Channel3_COUNT=$(($Card1_Channel3_COUNT + 1))
      echo "Stopped $Card1_Channel3_1_MAC_ADDR or $Card1_Channel3_2_MAC_ADDR. Mute GPIO$Card1_Channel3_GPIO. . Count: $Card1_Channel3_COUNT"
    fi
  fi

  ##############################################
  # Card 1 Channel 4
  ##############################################
  RESULT=`echo "$Card1_Channel4_1_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
  echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  # Wenn 1. Kanal nicht läuft, prüfen ob zweiter Kanal läuft
  if [ $? = 1 ]; then
        RESULT=`echo "$Card1_Channel4_2_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
        echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  fi

  if [ $? = 0 ]; then
    echo "Playing $Card1_Channel4_1_MAC_ADDR or $Card1_Channel4_2_MAC_ADDR. Unmute GPIO$Card1_Channel4_GPIO. Count: $Card1_Channel4_COUNT"
    Card1_Channel4_COUNT=0
    if [ $Card1_Channel4_TURNED_ON = 0 ]; then
        if [ $Card1_TURNED_ONPWR = 0 ]; then
          sudo echo "1" > /sys/class/gpio/gpio$Card1_GPIO_PWR/value
          Card1_TURNED_ONPWR=1
          echo "Power on!"
        fi
        sudo echo "1" > /sys/class/gpio/gpio$Card1_Channel4_GPIO/value
        Card1_Channel4_TURNED_ON=1
        echo "Turn on: $Card1_Channel4_GPIO"
    fi
  else
    if [ $Card1_Channel4_COUNT -ge $DELAYOFF ]; then
      if [ $Card1_Channel4_TURNED_ON = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel4_GPIO/value
        Card1_Channel4_TURNED_ON=0
        echo "Turn off: $Card1_Channel4_GPIO"
      fi
      Card1_Channel4_COUNT=0
    else
      Card1_Channel4_COUNT=$(($Card1_Channel4_COUNT + 1))
      echo "Stopped $Card1_Channel4_1_MAC_ADDR or $Card1_Channel4_2_MAC_ADDR. Mute GPIO$Card1_Channel4_GPIO. . Count: $Card1_Channel4_COUNT"
    fi
  fi

  ##############################################
  # Card 1 Power off
  ##############################################
  if [ $Card1_Channel1_TURNED_ON = 0 ] && [ $Card1_Channel2_TURNED_ON = 0 ] && [ $Card1_Channel3_TURNED_ON = 0 ] && [ $Card1_Channel4_TURNED_ON = 0 ]; then
    if [ $Card1_COUNTPWR -ge $Card1_DELAYOFFPWR ]; then
      if [ $Card1_TURNED_ONPWR = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card1_GPIO_PWR/value
        Card1_TURNED_ONPWR=0
        echo "Power off!"
      fi
      Card1_COUNTPWR=0
    else
      Card1_COUNTPWR=$(($Card1_COUNTPWR + 1))
      echo "All players stopped. Count power off: $Card1_COUNTPWR"
    fi 
  fi


  ##############################################
  # Card 2 Channel 1
  ##############################################
  RESULT=`echo "$Card2_Channel1_1_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
  echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  # Wenn 1. Kanal nicht läuft, prüfen ob zweiter Kanal läuft
  if [ $? = 1 ]; then
        RESULT=`echo "$Card2_Channel1_2_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
        echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  fi

  if [ $? = 0 ]; then
    echo "Playing $Card2_Channel1_1_MAC_ADDR or $Card2_Channel1_2_MAC_ADDR. Unmute GPIO$Card2_Channel1_GPIO. Count: $Card2_Channel1_COUNT"
    Card2_Channel1_COUNT=0
    if [ $Card2_Channel1_TURNED_ON = 0 ]; then
        if [ $Card2_TURNED_ONPWR = 0 ]; then
          sudo echo "1" > /sys/class/gpio/gpio$Card2_GPIO_PWR/value
          Card2_TURNED_ONPWR=1
          echo "Power on!"
        fi
        sudo echo "1" > /sys/class/gpio/gpio$Card2_Channel1_GPIO/value
        Card2_Channel1_TURNED_ON=1
        echo "Turn on: $Card2_Channel1_GPIO"
    fi
  else
    if [ $Card2_Channel1_COUNT -ge $DELAYOFF ]; then
      if [ $Card2_Channel1_TURNED_ON = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel1_GPIO/value
        Card2_Channel1_TURNED_ON=0
        echo "Turn off: $Card2_Channel1_GPIO"
      fi
      Card2_Channel1_COUNT=0
    else
      Card2_Channel1_COUNT=$(($Card2_Channel1_COUNT + 1))
      echo "Stopped $Card2_Channel1_1_MAC_ADDR or $Card2_Channel1_2_MAC_ADDR. Mute GPIO$Card2_Channel1_GPIO. . Count: $Card2_Channel1_COUNT"
    fi
  fi

  ##############################################
  # Card 2 Channel 2
  ##############################################
  RESULT=`echo "$Card2_Channel2_1_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
  echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  # Wenn 1. Kanal nicht läuft, prüfen ob zweiter Kanal läuft
  if [ $? = 1 ]; then
        RESULT=`echo "$Card2_Channel2_2_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
        echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  fi

  if [ $? = 0 ]; then
    echo "Playing $Card2_Channel2_1_MAC_ADDR or $Card2_Channel2_2_MAC_ADDR. Unmute GPIO$Card2_Channel2_GPIO. Count: $Card2_Channel2_COUNT"
    Card2_Channel2_COUNT=0
    if [ $Card2_Channel2_TURNED_ON = 0 ]; then
        if [ $Card2_TURNED_ONPWR = 0 ]; then
          sudo echo "1" > /sys/class/gpio/gpio$Card2_GPIO_PWR/value
          Card2_TURNED_ONPWR=1
          echo "Power on!"
        fi
        sudo echo "1" > /sys/class/gpio/gpio$Card2_Channel2_GPIO/value
        Card2_Channel2_TURNED_ON=1
        echo "Turn on: $Card2_Channel2_GPIO"
    fi
  else
    if [ $Card2_Channel2_COUNT -ge $DELAYOFF ]; then
      if [ $Card2_Channel2_TURNED_ON = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel2_GPIO/value
        Card2_Channel2_TURNED_ON=0
        echo "Turn off: $Card2_Channel2_GPIO"
      fi
      Card2_Channel2_COUNT=0
    else
      Card2_Channel2_COUNT=$(($Card2_Channel2_COUNT + 1))
      echo "Stopped $Card2_Channel2_1_MAC_ADDR or $Card2_Channel2_2_MAC_ADDR. Mute GPIO$Card2_Channel2_GPIO. . Count: $Card2_Channel2_COUNT"
    fi
  fi

  ##############################################
  # Card 2 Channel 3
  ##############################################
  RESULT=`echo "$Card2_Channel3_1_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
  echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  # Wenn 1. Kanal nicht läuft, prüfen ob zweiter Kanal läuft
  if [ $? = 1 ]; then
        RESULT=`echo "$Card2_Channel3_2_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
        echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  fi

  if [ $? = 0 ]; then
    echo "Playing $Card2_Channel3_1_MAC_ADDR or $Card2_Channel3_2_MAC_ADDR. Unmute GPIO$Card2_Channel3_GPIO. Count: $Card2_Channel3_COUNT"
    Card2_Channel3_COUNT=0
    if [ $Card2_Channel3_TURNED_ON = 0 ]; then
        if [ $Card2_TURNED_ONPWR = 0 ]; then
          sudo echo "1" > /sys/class/gpio/gpio$Card2_GPIO_PWR/value
          Card2_TURNED_ONPWR=1
          echo "Power on!"
        fi
        sudo echo "1" > /sys/class/gpio/gpio$Card2_Channel3_GPIO/value
        Card2_Channel3_TURNED_ON=1
        echo "Turn on: $Card2_Channel3_GPIO"
    fi
  else
    if [ $Card2_Channel3_COUNT -ge $DELAYOFF ]; then
      if [ $Card2_Channel3_TURNED_ON = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel3_GPIO/value
        Card2_Channel3_TURNED_ON=0
        echo "Turn off: $Card2_Channel3_GPIO"
      fi
      Card2_Channel3_COUNT=0
    else
      Card2_Channel3_COUNT=$(($Card2_Channel3_COUNT + 1))
      echo "Stopped $Card2_Channel3_1_MAC_ADDR or $Card2_Channel3_2_MAC_ADDR. Mute GPIO$Card2_Channel3_GPIO. . Count: $Card2_Channel3_COUNT"
    fi
  fi

  ##############################################
  # Card 2 Channel 4
  ##############################################
  RESULT=`echo "$Card2_Channel4_1_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
  echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  # Wenn 1. Kanal nicht läuft, prüfen ob zweiter Kanal läuft
  if [ $? = 1 ]; then
        RESULT=`echo "$Card2_Channel4_2_MAC_ADDR $COMMAND" | timeout 0.1 nc $LMS_IP 9090`
        echo $RESULT | grep "mode%3Aplay" > /dev/null 2>&1
  fi

  if [ $? = 0 ]; then
    echo "Playing $Card2_Channel4_1_MAC_ADDR or $Card2_Channel4_2_MAC_ADDR. Unmute GPIO$Card2_Channel4_GPIO. Count: $Card2_Channel4_COUNT"
    Card2_Channel4_COUNT=0
    if [ $Card2_Channel4_TURNED_ON = 0 ]; then
        if [ $Card2_TURNED_ONPWR = 0 ]; then
          sudo echo "1" > /sys/class/gpio/gpio$Card2_GPIO_PWR/value
          Card2_TURNED_ONPWR=1
          echo "Power on!"
        fi
        sudo echo "1" > /sys/class/gpio/gpio$Card2_Channel4_GPIO/value
        Card2_Channel4_TURNED_ON=1
        echo "Turn on: $Card2_Channel4_GPIO"
    fi
  else
    if [ $Card2_Channel4_COUNT -ge $DELAYOFF ]; then
      if [ $Card2_Channel4_TURNED_ON = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel4_GPIO/value
        Card2_Channel4_TURNED_ON=0
        echo "Turn off: $Card2_Channel4_GPIO"
      fi
      Card2_Channel4_COUNT=0
    else
      Card2_Channel4_COUNT=$(($Card2_Channel4_COUNT + 1))
      echo "Stopped $Card2_Channel4_1_MAC_ADDR or $Card2_Channel4_2_MAC_ADDR. Mute GPIO$Card2_Channel4_GPIO. . Count: $Card2_Channel4_COUNT"
    fi
  fi

  ##############################################
  # Card 2 Power off
  ##############################################
  if [ $Card2_Channel1_TURNED_ON = 0 ] && [ $Card2_Channel2_TURNED_ON = 0 ] && [ $Card2_Channel3_TURNED_ON = 0 ] && [ $Card2_Channel4_TURNED_ON = 0 ]; then
    if [ $Card2_COUNTPWR -ge $Card2_DELAYOFFPWR ]; then
      if [ $Card2_TURNED_ONPWR = 1 ]; then
        sudo echo "0" > /sys/class/gpio/gpio$Card2_GPIO_PWR/value
        Card2_TURNED_ONPWR=0
        echo "Power off!"
      fi
      Card2_COUNTPWR=0
    else
      Card2_COUNTPWR=$(($Card2_COUNTPWR + 1))
      echo "All players stopped. Count power off: $Card2_COUNTPWR"
    fi 
  fi
}


##############################################
# Initial GPIO setup
##############################################

sudo chown tc /sys/class/gpio/export

# Card 1

sudo echo $Card1_Channel1_GPIO > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card1_Channel1_GPIO/
sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel1_GPIO/value
sudo echo "out" > /sys/class/gpio/gpio$Card1_Channel1_GPIO/direction
sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel1_GPIO/value

sudo echo $Card1_Channel2_GPIO > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card1_Channel2_GPIO/
sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel2_GPIO/value
sudo echo "out" > /sys/class/gpio/gpio$Card1_Channel2_GPIO/direction
sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel2_GPIO/value

sudo echo $Card1_Channel3_GPIO > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card1_Channel3_GPIO/
sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel3_GPIO/value
sudo echo "out" > /sys/class/gpio/gpio$Card1_Channel3_GPIO/direction
sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel3_GPIO/value

sudo echo $Card1_Channel4_GPIO > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card1_Channel4_GPIO/
sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel4_GPIO/value
sudo echo "out" > /sys/class/gpio/gpio$Card1_Channel4_GPIO/direction
sudo echo "0" > /sys/class/gpio/gpio$Card1_Channel4_GPIO/value

sudo echo $Card1_GPIO_PWR > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card1_GPIO_PWR/
sudo echo "0" > /sys/class/gpio/gpio$Card1_GPIO_PWR/value
sudo echo "out" > /sys/class/gpio/gpio$Card1_GPIO_PWR/direction
sudo echo "0" > /sys/class/gpio/gpio$Card1_GPIO_PWR/value

# Card 2

sudo echo $Card2_Channel1_GPIO > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card2_Channel1_GPIO/
sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel1_GPIO/value
sudo echo "out" > /sys/class/gpio/gpio$Card2_Channel1_GPIO/direction
sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel1_GPIO/value

sudo echo $Card2_Channel2_GPIO > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card2_Channel2_GPIO/
sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel2_GPIO/value
sudo echo "out" > /sys/class/gpio/gpio$Card2_Channel2_GPIO/direction
sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel2_GPIO/value

sudo echo $Card2_Channel3_GPIO > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card2_Channel3_GPIO/
sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel3_GPIO/value
sudo echo "out" > /sys/class/gpio/gpio$Card2_Channel3_GPIO/direction
sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel3_GPIO/value

sudo echo $Card2_Channel4_GPIO > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card2_Channel4_GPIO/
sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel4_GPIO/value
sudo echo "out" > /sys/class/gpio/gpio$Card2_Channel4_GPIO/direction
sudo echo "0" > /sys/class/gpio/gpio$Card2_Channel4_GPIO/value

sudo echo $Card2_GPIO_PWR > /sys/class/gpio/export
sudo chown -R tc /sys/class/gpio/gpio$Card2_GPIO_PWR/
sudo echo "0" > /sys/class/gpio/gpio$Card2_GPIO_PWR/value
sudo echo "out" > /sys/class/gpio/gpio$Card2_GPIO_PWR/direction
sudo echo "0" > /sys/class/gpio/gpio$Card2_GPIO_PWR/value

##############################################
# Loop forever. This uses less than 1% CPU, so it should be OK.
##############################################
while true
  do
    get_mode
    sleep $INTERVAL
  done

