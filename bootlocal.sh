#!/bin/sh
# put other system startup commands here

. /home/tc/muting_power.sh &
/opt/eth0.sh

# Wiederherstellen der alsamixer-Einstellungen(Lautstaerke)
sudo alsactl restore


# start SqeezeLite-Instanzen für asound.conf.eight fuer 1 Karte
#/usr/local/bin/squeezelite -n "Essen" -o stereofront -a 80:::0: -m ab:cd:ef:12:34:60 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
#/usr/local/bin/squeezelite -n "Kochen" -o stereosurround -a 80:::0: -m ab:cd:ef:12:34:62 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
#/usr/local/bin/squeezelite -n "Gang EG/WC" -o stereocenter1 -a 80:::0: -m ab:cd:ef:12:34:64 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
#/usr/local/bin/squeezelite -n "Gang EG" -o monocenterr -a 80:::0: -m ab:cd:ef:12:34:64 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
#/usr/local/bin/squeezelite -n "WC" -o monocenterl -a 80:::0: -m ab:cd:ef:12:34:65 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
#/usr/local/bin/squeezelite -n "Benjamin" -o monobackl -a 80:::0: -m ab:cd:ef:12:34:66 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
#/usr/local/bin/squeezelite -n "Elena" -o monobackr -a 80:::0: -m ab:cd:ef:12:34:67 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log

# start SqeezeLite-Instanzen
# Karte 1
/usr/local/bin/squeezelite -n "Essen" -o card1_stereofront -a 80:::0: -m ab:cd:ef:12:34:60 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "Kochen" -o card1_stereosurround -a 80:::0: -m ab:cd:ef:12:34:62 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
#/usr/local/bin/squeezelite -n "Gang EG/WC" -o card1_stereocenter -a 80:::0: -m ab:cd:ef:12:34:64 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "Gang EG" -o card1_monocenterr -a 80:::0: -m ab:cd:ef:12:34:64 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "WC" -o card1_monocenterl -a 80:::0: -m ab:cd:ef:12:34:65 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "Benjamin" -o card1_monobackl -a 80:::0: -m ab:cd:ef:12:34:66 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "Elena" -o card1_monobackr -a 80:::0: -m ab:cd:ef:12:34:67 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
# Karte 2
/usr/local/bin/squeezelite -n "Schlafen" -o card2_monofrontl -a 80:::0: -m ab:cd:ef:12:34:70 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "Bad" -o card2_monofrontl -a 80:::0: -m ab:cd:ef:12:34:71 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "Buero" -o card2_monosurroundr -a 80:::0: -m ab:cd:ef:12:34:72 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
#/usr/local/bin/squeezelite -n "Reserve DG" -o card2_monosurroundl -a 80:::0: -m ab:cd:ef:12:34:73 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "Waschen" -o card2_monocenterl -a 80:::0: -m ab:cd:ef:12:34:74 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "Hobby" -o card2_monocenterr -a 80:::0: -m ab:cd:ef:12:34:75 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
/usr/local/bin/squeezelite -n "Party" -o card2_monobackl -a 80:::0: -m ab:cd:ef:12:34:76 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log
#/usr/local/bin/squeezelite -n "Reserve KG" -o card2_monobackr -a 80:::0: -m ab:cd:ef:12:34:77 -C 1 -z -s logitechmediaserver -d all=debug -f /var/log/squeezelite_instances.log


GREEN="$(echo -e '\033[1;32m')"

echo
echo "${GREEN}Running bootlocal.sh..."
#pCPstart------
/usr/local/etc/init.d/pcp_startup.sh 2>&1 | tee -a /var/log/pcp_boot.log
#pCPstop------
