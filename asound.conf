##########################################################################
## CUSTOM ASOUND.CONF by nEiMi
##########################################################################
## Channel splitting your UA0099 USB sound card to get up to four
## independent stereo outputs (...and more )
##########################################################################
##
## for the first USB-Souncard:
##
## stereofront1 = the FRONT labeled green jack
## stereosurround1 = the SURROUND labeled black jack
## stereocenter1 = the CENTER / BASS labeled yellow jack
## stereoback1 = the BACK labeled black jack
## stereoall1 = all stereo outputs
##
## for the second USB-Souncard:
##
## stereofront2 = the FRONT labeled green jack
## stereosurround2 = the SURROUND labeled black jack
## stereocenter2 = the CENTER / BASS labeled yellow jack
## stereoback2 = the BACK labeled black jack
## stereoall2 = all stereo outputs
##
##########################################################################


##########################################################################
## labels and colors could be different for differnt vendors or
## versions of the device (Logilink, Sewell, Sweex, ...)
##########################################################################
## put this file into your /etc/asound.conf
##########################################################################

pcm.snd_card1 {
    type hw
    card 1
    device 0
}

ctl.snd_card1 {
    type hw
    card 1
    device 0
}

pcm.snd_card2 {
    type hw
    card 2
    device 0
}

ctl.snd_card2 {
    type hw
    card 2
    device 0
}

pcm.dmixer1 {
    type dmix
    ipc_key 1024                            # must be unique number on your system
    ipc_perm 0666                           # neccesary for normal user to have access
    slave.pcm "snd_card1"
    slave {
        period_time 0
        period_size 1024
        buffer_size 4096
        rate 44100
        channels 8
    }
    bindings {
        0 0
        1 1
        2 2
        3 3
        4 4
        5 5
        6 6
        7 7
    }
}

pcm.dmixer2 {
    type dmix
    ipc_key 1024
    ipc_perm 0666
    slave.pcm "snd_card2"
    slave {
        period_time 0
        period_size 1024
        buffer_size 4096
        rate 44100
        channels 8
    }
    bindings {
        0 0
        1 1
        2 2
        3 3
        4 4
        5 5
        6 6
        7 7
    }
}

pcm.out {
    type plug
    slave.pcm {
        type multi
        slaves {
            a {
                channels 8
                pcm "dmixer1"
            }
            b {
                channels 8
                pcm "dmixer2"
            }
        }
        bindings {
            0 { slave a channel 0 }
            1 { slave a channel 1 }
            2 { slave a channel 2 }
            3 { slave a channel 3 }
            4 { slave a channel 4 }
            5 { slave a channel 5 }
            6 { slave a channel 6 }
            7 { slave a channel 7 }
            8 { slave b channel 0 }
            9 { slave b channel 1 }
            10 { slave b channel 2 }
            11 { slave b channel 3 }
            12 { slave b channel 4 }
            13 { slave b channel 5 }
            14 { slave b channel 6 }
            15 { slave b channel 7 }
        }
    }
}


pcm.!default {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.0 1
    ttable.1.1 1
    ttable.0.2 1
    ttable.1.3 1
    ttable.0.4 1
    ttable.1.5 1
    ttable.0.6 1
    ttable.1.7 1
    ttable.0.8 1
    ttable.1.9 1
    ttable.0.10 1
    ttable.1.11 1
    ttable.0.12 1
    ttable.1.13 1
    ttable.0.14 1
    ttable.1.15 1
}

##########################################################################
## USB-Soundcard 1
##########################################################################

pcm.card1_stereofront {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.0 1
    ttable.1.1 1
}

pcm.card1_monofrontl {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.0 1 # in-channel 0, out-channel 0, 100% volume
    ttable.1.0 1 # in-channel 1, out-channel 0, 100% volume
}

pcm.card1_monofrontr {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.1 1
    ttable.1.1 1
}

pcm.card1_stereocenter {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.2 1
    ttable.1.3 1
}

pcm.card1_monocenterl {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.2 1
    ttable.1.2 1
}

pcm.card1_monocenterr {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.3 1
    ttable.1.3 1
}

pcm.card1_stereoback {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.4 1
    ttable.1.5 1
}

pcm.card1_monobackl {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.4 1
    ttable.1.4 1
}

pcm.card1_monobackr {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.5 1
    ttable.1.5 1
}

pcm.card1_stereosurround {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.6 1
    ttable.1.7 1
}

pcm.card1_monosurroundl {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.6 1
    ttable.1.6 1
}

pcm.card1_monosurroundr {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.7 1
    ttable.1.7 1
}

##########################################################################
## USB-Soundcard 2
##########################################################################

pcm.card2_stereofront {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.8 1
    ttable.1.9 1
}

pcm.card2_monofrontl {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.8 1 # in-channel 0, out-channel 0, 100% volume
    ttable.1.8 1 # in-channel 1, out-channel 0, 100% volume
}

pcm.card2_monofrontr {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.9 1
    ttable.1.9 1
}

pcm.card2_stereocenter {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.10 1
    ttable.1.11 1
}

pcm.card2_monocenterl {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.10 1
    ttable.1.10 1
}

pcm.card2_monocenterr {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.11 1
    ttable.1.11 1
}

pcm.card2_stereoback {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.12 1
    ttable.1.13 1
}

pcm.card2_monobackl {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.12 1
    ttable.1.12 1
}

pcm.card2_monobackr {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.13 1
    ttable.1.13 1
}

pcm.card2_stereosurround {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.14 1
    ttable.1.15 1
}

pcm.card2_monosurroundl {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.14 1
    ttable.1.14 1
}

pcm.card2_monosurroundr {
    type route
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.15 1
    ttable.1.15 1
}

##########################################################################
## ALLE
##########################################################################

pcm.alle {
    type plug
    slave {
        pcm "out"
        channels 16
    }
    ttable.0.0 1
    ttable.1.1 1
    ttable.0.2 1
    ttable.1.3 1
    ttable.0.4 1
    ttable.1.5 1
    ttable.0.6 1
    ttable.1.7 1
    ttable.0.8 1
    ttable.1.9 1
    ttable.0.10 1
    ttable.1.11 1
    ttable.0.12 1
    ttable.1.13 1
    ttable.0.14 1
    ttable.1.15 1
}